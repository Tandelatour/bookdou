﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookDou.Models.Annotations
{
    /// <summary>
    /// Methode d'annotion pour verifier sir li'isbn est dans la base actuelle 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
     public class VerificationIsbnExisteAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Console.WriteLine("Annotations perso");

            if (value is string isbn)
            {

                Console.WriteLine("Annotations perso -entrer dans le if string isbn");

                var dbContext = (BookdoubddContext)validationContext.GetService(typeof(BookdoubddContext));

                bool isbnExists = dbContext.Livres.Any(l => l.Isbn == isbn);
                Console.WriteLine("Annotation perso - " + isbnExists);
                if (isbnExists)
                {
                    return new ValidationResult("Cet ISBN existe déjà dans la base de données.");
                }
            }
            Console.WriteLine("Annotations perso - reussite " );
            return ValidationResult.Success;
        }
    }
}

