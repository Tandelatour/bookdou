﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace BookDou.Models;

public partial class BookdoubddContext : DbContext
{
    public BookdoubddContext()
    {
    }

    public BookdoubddContext(DbContextOptions<BookdoubddContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Collection> Collections { get; set; }

    public virtual DbSet<CollectionLivre> CollectionLivres { get; set; }

    public virtual DbSet<Genre> Genres { get; set; }

    public virtual DbSet<Livre> Livres { get; set; }

    public virtual DbSet<Utilisateur> Utilisateurs { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseNpgsql("Host=localhost;Database=bookdoubdd;Port=5432;Username=michelbookdou;Password=chiensaucisse");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Collection>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("collection_pkey");

            entity.ToTable("collection");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.DateCreation)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_creation");
            entity.Property(e => e.DateModification)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_modification");
            entity.Property(e => e.DateSuppression)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_suppression");
            entity.Property(e => e.Description).HasColumnName("description");
            entity.Property(e => e.Nom)
                .HasMaxLength(50)
                .HasColumnName("nom");
            entity.Property(e => e.UtilisateurId).HasColumnName("utilisateur_id");

            entity.HasOne(d => d.Utilisateur).WithMany(p => p.Collections)
                .HasForeignKey(d => d.UtilisateurId)
                .HasConstraintName("fk_collection_utilisateur");
        });

        modelBuilder.Entity<CollectionLivre>(entity =>
        {
            entity.HasKey(e => new { e.LivreId, e.CollectionId }).HasName("collection_livre_pkey");

            entity.ToTable("collection_livre");

            entity.Property(e => e.LivreId).HasColumnName("livre_id");
            entity.Property(e => e.CollectionId).HasColumnName("collection_id");
            entity.Property(e => e.Lu).HasColumnName("lu");
            entity.Property(e => e.DateCreation)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_creation");
            entity.Property(e => e.DateModification)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_modification");
            entity.Property(e => e.DateSuppression)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_suppression");

            entity.HasOne(d => d.Collection).WithMany(p => p.CollectionLivres)
                .HasForeignKey(d => d.CollectionId)
                .HasConstraintName("fk_collection_livre_collection");

            entity.HasOne(d => d.Livre).WithMany(p => p.CollectionLivres)
                .HasForeignKey(d => d.LivreId)
                .HasConstraintName("fk_collection_livre_livre");
        });

        modelBuilder.Entity<Genre>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("genre_pkey");

            entity.ToTable("genre");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Libelle)
                .HasMaxLength(50)
                .HasColumnName("libelle");

            entity.HasMany(d => d.Livres).WithMany(p => p.Genres)
                .UsingEntity<Dictionary<string, object>>(
                    "GenreLivre",
                    r => r.HasOne<Livre>().WithMany()
                        .HasForeignKey("LivreId")
                        .HasConstraintName("fk_genre_livre_livre"),
                    l => l.HasOne<Genre>().WithMany()
                        .HasForeignKey("GenreId")
                        .HasConstraintName("fk_genre_livre_genre"),
                    j =>
                    {
                        j.HasKey("GenreId", "LivreId").HasName("genre_livre_pkey");
                        j.ToTable("genre_livre");
                        j.IndexerProperty<int>("GenreId").HasColumnName("genre_id");
                        j.IndexerProperty<int>("LivreId").HasColumnName("livre_id");
                    });
        });

        modelBuilder.Entity<Livre>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("livre_pkey");

            entity.ToTable("livre");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Auteur)
                .HasMaxLength(80)
                .HasColumnName("auteur");
            entity.Property(e => e.DateCreation)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_creation");
            entity.Property(e => e.DateDePublication)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_de_publication");
            entity.Property(e => e.DateModification)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_modification");
            entity.Property(e => e.DateSuppression)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_suppression");
            entity.Property(e => e.Description).HasColumnName("description");
            entity.Property(e => e.Editeur)
                .HasMaxLength(50)
                .HasColumnName("editeur");
            entity.Property(e => e.Image)
                .HasColumnType("character varying")
                .HasColumnName("image");
            entity.Property(e => e.Isbn)
                .HasMaxLength(13)
                .HasColumnName("isbn");
        
            entity.Property(e => e.Titre)
                .HasMaxLength(100)
                .HasColumnName("titre");

            entity.HasIndex(e => e.Isbn)
                 .IsUnique();
        });

        modelBuilder.Entity<Utilisateur>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("utilisateur_pkey");

            entity.ToTable("utilisateur");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.DateCreation)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_creation");
            entity.Property(e => e.DateModification)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_modification");
            entity.Property(e => e.DateSuppression)
                .HasColumnType("timestamp without time zone")
                .HasColumnName("date_suppression");
            entity.Property(e => e.Email)
                .HasMaxLength(50)
                .HasColumnName("email");
            entity.Property(e => e.ImageProfil)
                .HasColumnType("character varying")
                .HasColumnName("image_profil");
            entity.Property(e => e.Login)
                .HasMaxLength(50)
                .HasColumnName("login");
            entity.Property(e => e.MotDePasse)
                .HasMaxLength(100)
                .HasColumnName("mot_de_passe");
            entity.Property(e => e.Nom)
                .HasMaxLength(50)
                .HasColumnName("nom");
            entity.Property(e => e.Prenom)
                .HasMaxLength(50)
                .HasColumnName("prenom");
            entity.HasIndex(e => e.Login)
                .IsUnique();
            entity.HasIndex(e => e.Email)
                .IsUnique();
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
