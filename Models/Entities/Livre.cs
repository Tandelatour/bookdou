﻿    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Security.Policy;
    using BookDou.Models.Annotations;
    using Microsoft.EntityFrameworkCore;

    namespace BookDou.Models;

    /// <summary>
    /// Représente un livre avec des informations telles que l'ID, l'ISBN,
    /// le titre, l'auteur, l'éditeur, les dates de publication et d'autres attributs.
    /// </summary>

    //ajout de ce champ pour la contrainte sur l'unicité de l'isbn
    [Index(nameof(Isbn), IsUnique = true)]
    public partial class Livre
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Le champ ISBN est obligatoire.")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Veuillez saisir uniquement des chiffres.")]
        [StringLength(13, MinimumLength = 10, ErrorMessage = "L'ISBN doit comporter entre 10 et 13 caractères.")]
        // vu que je dois pouvoir editer cette methode n'est pas utile..
        //  c'est plutôt une condition dans le controlleur
        // [VerificationIsbnExiste]   
        public string Isbn { get; set; } = null!;

        [Required(ErrorMessage = "Le champ Titre est obligatoire.")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Le champ Titre doit être compris entre 1 et 100 caractères.")]
        public string Titre { get; set; } = null!;

        [Display(Name = "Nom de l'auteur.e")]
        [Required(ErrorMessage = "Le champ Nom de l'auteur.e est obligatoire.")]
        [StringLength(80, MinimumLength = 1, ErrorMessage = "Le champ Auteur.e doit être compris entre 1 et 80 caractères.")]
        public string Auteur { get; set; } = null!;

        [Display(Name = "Prénom de l'auteur.e")]
        [NotMapped]
        public string? AuteurPrenom { get; set; }

        [Display(Name= "Éditeur")]
        [Required(ErrorMessage = "Le champ Éditeur est obligatoire.")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Le champ Éditeur doit être compris entre 1 et 50 caractères.")]
        public string Editeur { get; set; } = null!;

        [Display(Name = "Date de Publication")]
        [DataType(DataType.Date)]
        public DateTime? DateDePublication { get; set; }

        [DataType(DataType.MultilineText)]
        public string? Description { get; set; }

        [Display(Name = "Image de Couverture")]
        public string? Image { get; set; }

        public DateTime DateCreation { get; set; }

        public DateTime DateModification { get; set; }

        public DateTime? DateSuppression { get; set; }

        public virtual ICollection<CollectionLivre> CollectionLivres { get; set; } = new List<CollectionLivre>();

        public virtual ICollection<Genre> Genres { get; set; } = new List<Genre>();


    }
