﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookDou.Models;

public partial class Collection
{
    public int Id { get; set; }

    [Required(ErrorMessage = "Le champ Nom est obligatoire.")]
    [StringLength(50, MinimumLength = 1, ErrorMessage = "Le champ Nom doit être compris entre 1 et 50 caractères.")]
    public string Nom { get; set; } = null!;

    public string? Description { get; set; }

    public int UtilisateurId { get; set; }

    public DateTime DateCreation { get; set; }

    public DateTime DateModification { get; set; }

    public DateTime? DateSuppression { get; set; }

    public virtual ICollection<CollectionLivre> CollectionLivres { get; set; } = new List<CollectionLivre>();

    public virtual Utilisateur?  Utilisateur { get; set; }
}
