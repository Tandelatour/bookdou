﻿using System;
using System.Collections.Generic;

namespace BookDou.Models;

public partial class Genre
{
    public int Id { get; set; }

    public string Libelle { get; set; } = null!;

    public virtual ICollection<Livre> Livres { get; set; } = new List<Livre>();
}
