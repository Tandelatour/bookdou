﻿using System;
using System.Collections.Generic;

namespace BookDou.Models;

public partial class CollectionLivre
{
    public int CollectionId { get; set; }

    public int LivreId { get; set; }

    public bool Lu { get; set; }

    public DateTime DateCreation { get; set; }

    public DateTime DateModification { get; set; }

    public DateTime? DateSuppression { get; set; }

    public virtual Collection Collection { get; set; } = null!;

    public virtual Livre Livre { get; set; } = null!;
}
