﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace BookDou.Models;

/// <summary>
/// Représente un utilisateur avec des informations telles que l'ID, l'email, le login,
/// le mot de passe, le nom, le prénom, l'image de profil, etc.
/// </summary>
[Index(nameof(Email), IsUnique = true)]
[Index(nameof(Login), IsUnique = true)]
public partial class Utilisateur
{
    public int Id { get; set; }


    [Required(ErrorMessage = "Le champ Email est obligatoire.")]
    [EmailAddress(ErrorMessage = "Veuillez entrer une adresse email valide")]
    public string Email { get; set; } = null!;

    [Required(ErrorMessage = "Le champ Login est obligatoire.")]
    [StringLength(50, MinimumLength = 3, ErrorMessage = "Le champ Login doit être compris entre 3 et 50 caractères.")]
    [RegularExpression(@"^(?=.*[A-Za-z0-9]$)[A-Za-z][A-Za-z\d.-]{0,}$",
    ErrorMessage = "\"Le champ Login doit commencer par une lettre, ne doit contenir que des lettres, des chiffres, des points et des tirets, et doit se terminer par une lettre ou un chiffre. De plus, il ne doit pas contenir d'espaces.\"")]
    public string Login { get; set; } = null!;

    [Display(Name = "Mot de passe")]
    [DataType(DataType.Password)]
    [Required(ErrorMessage = "Le champ Mot de Passe est obligatoire.")]
    [StringLength(50, MinimumLength = 6, ErrorMessage = "Le champ Mot de passe doit être compris entre 6 et 50 caractères.")]
    public string MotDePasse { get; set; } = null!;

    [Display(Name = "Confirmer le mot de passe")]
    [DataType(DataType.Password)]
    [Required(ErrorMessage = "Vous devez confirmez le Mot de Passe.")]
    [Compare("MotDePasse", ErrorMessage = "La confirmation du mot de passe ne correspond pas.")]
    [NotMapped]  //permet de ne pas ajouter dans la bdd et donc d'utiliser des classe pour de la logique metier 
    public string ConfirmerLeMotDePasse { get; set; } = null!;

    [StringLength(50, MinimumLength = 3, ErrorMessage = "Le champ Nom doit être compris entre 3 et 50 caractères.")]
    [RegularExpression(@"^(?![-'])[A-Za-zÀ-ÿ][-'A-Za-zÀ-ÿ ]*[^-'\s]$", ErrorMessage = "Le nom doit contenir uniquement des lettres, des caractères accentués, des tirets, des apostrophes et des espaces.")]
    public string? Nom { get; set; }


    [StringLength(50, MinimumLength = 3, ErrorMessage = "Le champ Prénom doit être compris entre 3 et 50 caractères.")]
    [RegularExpression(@"^(?![-'])[A-Za-zÀ-ÿ][-'A-Za-zÀ-ÿ ]*[^-'\s]$", ErrorMessage = "Le prénom doit contenir uniquement des lettres, des caractères accentués, des tirets, des apostrophes et des espaces.")]
    [Display(Name = "Prénom")]
    public string? Prenom { get; set; }


    //TODO ajouter regex pour verifier si c'est un lien image valide 
    [Display(Name = "Image de Profil")]
    public string? ImageProfil { get; set; }


    public DateTime DateCreation { get; set; }

    public DateTime DateModification { get; set; }

    public DateTime? DateSuppression { get; set; }

    public virtual ICollection<Collection> Collections { get; set; } = new List<Collection>();
}
