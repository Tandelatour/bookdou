﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using BookDou.Models;
using Newtonsoft.Json;

namespace BookDou.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {

        string utilisateurJson = HttpContext.Session.GetString("Utilisateur");
        if (utilisateurJson != null)
        {
            Utilisateur utilisateurConnecte = JsonConvert.DeserializeObject<Utilisateur>(utilisateurJson);

            return View(utilisateurConnecte);
        }
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}

