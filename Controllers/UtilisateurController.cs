using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BookDou.Models;
using BCrypt.Net;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using BookDou.Services;
using System.Text.RegularExpressions;
using Microsoft.Extensions.FileSystemGlobbing.Internal;

namespace BookDou.Controllers
{
    public class UtilisateurController : Controller
    {
        private readonly BookdoubddContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UtilisateurController(BookdoubddContext context)
        {
            _context = context;
        }

        // GET: Utilisateur
        public async Task<IActionResult> Index()
        {
            return _context.Utilisateurs != null ?
                        View(await _context.Utilisateurs.ToListAsync()) :
                        Problem("Entity set 'BookdoubddContext.Utilisateurs'  is null.");
        }

        // GET: Utilisateur/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Utilisateurs == null)
            {
                return NotFound();
            }

            var utilisateur = await _context.Utilisateurs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (utilisateur == null)
            {
                return NotFound();
            }

            return View(utilisateur);
        }

        // GET: Utilisateur/InscriptionUtilisateur
        public IActionResult InscriptionUtilisateur()
        {
            return View();
        }

        // POST: Utilisateur/InscriptionUtilisateur
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> InscriptionUtilisateur([Bind("Id,Email,Login,MotDePasse,ConfirmerLeMotDePasse")] Utilisateur utilisateur)
        {
            // Le modelstate est true ou false ou moment du Bind
            if (ModelState.IsValid)
            {
                var utilisateurExistant = await _context.Utilisateurs.FirstOrDefaultAsync(u => u.Login == utilisateur.Login || u.Email == utilisateur.Email);

                if (utilisateurExistant != null)
                {
                    if (utilisateurExistant.Login == utilisateur.Login)
                    {
                        ModelState.AddModelError(nameof(utilisateur.Login), "Un utilisateur avec ce login existe déjà.");
                    }

                    if (utilisateurExistant.Email == utilisateur.Email)
                    {
                        ModelState.AddModelError(nameof(utilisateur.Email), "Un utilisateur avec cet Email existe déjà.");
                    }

                    return View(utilisateur);
                }

                string hashMotDePasse = BCrypt.Net.BCrypt.HashPassword(utilisateur.MotDePasse);

                utilisateur.MotDePasse = hashMotDePasse;
                utilisateur.DateCreation = DateTime.Now;
                utilisateur.DateModification = DateTime.Now;

                _context.Add(utilisateur);

                // Méthode asyncr on lui deande d'attendre d'avoir un retour pour passer a la suite
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(utilisateur);
        }





        // GET: Utilisateur/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Utilisateurs == null)
            {
                return NotFound();
            }

            var utilisateur = await _context.Utilisateurs.FindAsync(id);
            if (utilisateur == null)
            {
                return NotFound();
            }
            return View(utilisateur);
        }

        // POST: Utilisateur/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Email,Login,MotDePasse,Nom,Prenom,ImageProfil,DateCreation,DateModification,DateSuppression")] Utilisateur utilisateur)
        {
            if (id != utilisateur.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(utilisateur);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UtilisateurExists(utilisateur.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(utilisateur);
        }

        // GET: Utilisateur/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Utilisateurs == null)
            {
                return NotFound();
            }

            var utilisateur = await _context.Utilisateurs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (utilisateur == null)
            {
                return NotFound();
            }

            return View(utilisateur);
        }

        // POST: Utilisateur/Delete/5
        [HttpPost, ActionName("Delete")]
 
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Utilisateurs == null)
            {
                return Problem("Entity set 'BookdoubddContext.Utilisateurs'  is null.");
            }
            var utilisateur = await _context.Utilisateurs.FindAsync(id);
            if (utilisateur != null)
            {
                _context.Utilisateurs.Remove(utilisateur);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UtilisateurExists(int id)
        {
            return (_context.Utilisateurs?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        ///     Affiche la vue connexion
        /// </summary>
        /// <remarks>GET: Utilisateur/Connexion</remarks>
        public IActionResult Connexion()
        {
            return View();
        }

        /// <summary>
        ///     Traite la demande de connexion d'un utilisateur.
        /// </summary>
        /// <remarks>POST: Utilisateur/Connexion</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Connexion([Bind("LoginEmail,MotDePasse")] UtilisateurConnexionService utilisateur)
        {
            if (ModelState.IsValid)
            {
                var utilisateurCourant = await _context.Utilisateurs.FirstOrDefaultAsync(u => u.Login == utilisateur.LoginEmail || u.Email == utilisateur.LoginEmail);

                if (utilisateurCourant != null)
                {
                    // Vérification du mot de passe en utilisant BCrypt.Net
                    bool motDePasseValide = BCrypt.Net.BCrypt.Verify(utilisateur.MotDePasse, utilisateurCourant.MotDePasse);

                    if (motDePasseValide)
                    {
                        Utilisateur utilisateurConnecte = utilisateurCourant;

                        // Sérialisation de l'objet utilisateurConnecte au format JSON
                        string utilisateurJson = JsonConvert.SerializeObject(utilisateurConnecte);

                        // Stockage de l'objet utilisateurJson dans la session
                        HttpContext.Session.SetString("Utilisateur", utilisateurJson);

                        // Redirection vers la page d'accueil après une connexion réussie
                        return RedirectToAction("Index", "Home");
                    }
                }

                ViewData["MessageErreur"] = "Utilisateur non trouvé ou mot de passe incorrect.";
            }

            return View(utilisateur);
        }


    }








}
