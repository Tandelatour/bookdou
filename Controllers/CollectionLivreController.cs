    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BookDou.Models;

namespace BookDou.Controllers
{
    public class CollectionLivreController : Controller
    {
        private readonly BookdoubddContext _context;

        public CollectionLivreController(BookdoubddContext context)
        {
            _context = context;
        }

        // GET: CollectionLivre
        public async Task<IActionResult> Index()
        {
            var bookdoubddContext = _context.CollectionLivres.Include(c => c.Collection).Include(c => c.Livre);
            return View(await bookdoubddContext.ToListAsync());
        }

        // GET: CollectionLivre/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.CollectionLivres == null)
            {
                return NotFound();
            }

            var collectionLivre = await _context.CollectionLivres
                .Include(c => c.Collection)
                .Include(c => c.Livre)
                .FirstOrDefaultAsync(m => m.LivreId == id);
            if (collectionLivre == null)
            {
                return NotFound();
            }

            return View(collectionLivre);
        }

        // GET: CollectionLivre/Create
        public IActionResult Create()
        {
            ViewData["CollectionId"] = new SelectList(_context.Collections, "Id", "Nom");
            ViewData["LivreId"] = new SelectList(_context.Livres, "Id", "Auteur");
            return View();
        }

        // POST: CollectionLivre/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CollectionId,LivreId,Lu,DateCreation,DateModification,DateSuppression")] CollectionLivre collectionLivre)
        {
            if (ModelState.IsValid)
            {
                _context.Add(collectionLivre);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CollectionId"] = new SelectList(_context.Collections, "Id", "Nom", collectionLivre.CollectionId);
            ViewData["LivreId"] = new SelectList(_context.Livres, "Id", "Auteur", collectionLivre.LivreId);
            return View(collectionLivre);
        }

        // GET: CollectionLivre/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.CollectionLivres == null)
            {
                return NotFound();
            }

            var collectionLivre = await _context.CollectionLivres.FindAsync(id);
            if (collectionLivre == null)
            {
                return NotFound();
            }
            ViewData["CollectionId"] = new SelectList(_context.Collections, "Id", "Nom", collectionLivre.CollectionId);
            ViewData["LivreId"] = new SelectList(_context.Livres, "Id", "Auteur", collectionLivre.LivreId);
            return View(collectionLivre);
        }

        // POST: CollectionLivre/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CollectionId,LivreId,Lu,DateCreation,DateModification,DateSuppression")] CollectionLivre collectionLivre)
        {
            if (id != collectionLivre.LivreId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(collectionLivre);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CollectionLivreExists(collectionLivre.LivreId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CollectionId"] = new SelectList(_context.Collections, "Id", "Nom", collectionLivre.CollectionId);
            ViewData["LivreId"] = new SelectList(_context.Livres, "Id", "Auteur", collectionLivre.LivreId);
            return View(collectionLivre);
        }

        // GET: CollectionLivre/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.CollectionLivres == null)
            {
                return NotFound();
            }

            var collectionLivre = await _context.CollectionLivres
                .Include(c => c.Collection)
                .Include(c => c.Livre)
                .FirstOrDefaultAsync(m => m.LivreId == id);
            if (collectionLivre == null)
            {
                return NotFound();
            }

            return View(collectionLivre);
        }

        // POST: CollectionLivre/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.CollectionLivres == null)
            {
                return Problem("Entity set 'BookdoubddContext.CollectionLivres'  is null.");
            }
            var collectionLivre = await _context.CollectionLivres.FindAsync(id);
            if (collectionLivre != null)
            {
                _context.CollectionLivres.Remove(collectionLivre);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CollectionLivreExists(int id)
        {
          return (_context.CollectionLivres?.Any(e => e.LivreId == id)).GetValueOrDefault();
        }
    }
}
