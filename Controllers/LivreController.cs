using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BookDou.Models;
using Newtonsoft.Json;
using Google.Apis.Books.v1;
using Google.Apis.Services;
using Google.Apis.Books.v1.Data;

namespace BookDou.Controllers
{
    public class LivreController : Controller
    {
        private readonly BookdoubddContext _context;

        public LivreController(BookdoubddContext context)
        {
            _context = context;
        }

        // GET: Livre
        public async Task<IActionResult> Index()
        {
            return _context.Livres != null ?
                        View(await _context.Livres.ToListAsync()) :
                        Problem("Entity set 'BookdoubddContext.Livres'  is null.");
        }

        // GET: Livre/Details/5
        public async Task<IActionResult> DetailsLivre(int? id)
        {
            if (id == null || _context.Livres == null)
            {
                return NotFound();
            }

            var livre = await _context.Livres
                .FirstOrDefaultAsync(m => m.Id == id);
            if (livre == null)
            {
                return NotFound();
            }

            return View(livre);
        }

        /// <summary>
        ///    Affiche la vue Ajouter un livre.
        /// </summary>
        /// <remarks>GET: Livre/AjouterLivre</remarks>
        public IActionResult AjouterLivre()
        {
            return View();
        }

        /// <summary>
        ///    Traite la recherche d'un livre à partir d'un code ISBN. Elle vérifie d'abord si le livre existe dans la base de données,
        ///    sinon elle utilise l'API Google Books pour récupérer les informations nécessaires.
        /// </summary>
        /// <remarks>POST: Livre/AjouterLivre</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AjouterLivre(Livre livre)
        {

            // 1. Vérifie si le livre existe déjà dans la base de données
            var livreExistant = await _context.Livres.FirstOrDefaultAsync(l => l.Isbn == livre.Isbn);

            if (livreExistant == null)
            {
                // 2. Si le livre n'est pas présent en base de données, utilise l'API Google Books
                // Initialisation du service d'accès à l'API Google Books 
                var service = new BooksService(new BaseClientService.Initializer { ApiKey = "AIzaSyAO19PGqvrPcDqVN8DH2mlo2Dl3gPX725I" });

                // Appel a l'API, récupération des information des livres a partir d'un code ISBN
                var volumes = await service.Volumes.List($"isbn:{livre.Isbn}").ExecuteAsync();

                // Aucun livre trouvé
                if (volumes.TotalItems == 0)
                {
                    ViewBag.Message = "Le livre n'a pas été trouvé.";
                    ViewBag.titreLivre = null;
                }
                else
                {
                    // Livre trouvé via l'API

                    // TODO : Si plusieurs livres sont trouvés, les afficher à l'utilisateur et le laisser sélectionner le plus pertinent
                    var livreGoogle = volumes.Items[0];

                    // Récupération des informations du livre depuis l'API
                    var titreApi = livreGoogle.VolumeInfo.Title;
                    var auteurApi = livreGoogle.VolumeInfo.Authors?.FirstOrDefault();
                    DateTime? datePublicationApi = DateTime.TryParse(livreGoogle.VolumeInfo.PublishedDate, out var datePublication) ? datePublication : null;
                    string editeurApi = livreGoogle.VolumeInfo.Publisher;
                    string descriptionApi = livreGoogle.VolumeInfo.Description;

                    // Vérification que toutes les informations nécessaires sont présentes
                    if (!string.IsNullOrEmpty(titreApi) && !string.IsNullOrEmpty(auteurApi) && datePublicationApi.HasValue)
                    {
                        // Crée une instance d'un livre avec toutes les informations récupérées
                        var livreApi = new Livre
                        {
                            Isbn = livre.Isbn,
                            Titre = titreApi,
                            Auteur = auteurApi,
                            DateDePublication = datePublicationApi,
                            Editeur = editeurApi ?? string.Empty,
                            Description = descriptionApi ?? string.Empty
                        };

                        // Stocke le livre trouvé via l'API dans la base de données
                        _context.Livres.Add(livreApi);

                        await _context.SaveChangesAsync();

                        ViewBag.titreLivre = "Le livre " + livreApi.Titre + " a été trouvé.";
                        ViewBag.idLivre = livreApi.Id;
                        ViewBag.Message = null;
                    }
                    else
                    {
                        // Redirige vers le formulaire pré-rempli si certaines informations sont manquantes
                        return RedirectToAction("AjouterLivreFormulairePrerempli", new
                        {
                            isbn = livre.Isbn,
                            titre = titreApi,
                            auteur = auteurApi,
                            datePublication = datePublicationApi,
                            editeur = editeurApi,
                            description = descriptionApi
                        });
                    }
                }

            }
            // Le livre est déjà présent en base de données
            else
            {
                ViewBag.MessageSucces = "Le livre " + livreExistant.Titre + " a été trouvé.";
                ViewBag.idLivre = livreExistant.Id;
                ViewBag.Message = null;
            }

            // Récupération de l'utilisateur connecté et de ses collections
            Utilisateur utilisateurConnecte = JsonConvert.DeserializeObject<Utilisateur>(HttpContext.Session.GetString("Utilisateur"));
            var collections = _context.Collections
                .Where(c => c.UtilisateurId == utilisateurConnecte.Id && c.DateSuppression == null)
                .ToList();
            ViewBag.Collections = collections;

            return View();
        }




        // POST: Livre/AjouterAMaCollection
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AjouterAMaCollection(int collectionId, int livreId)
        {
            var collection = await _context.Collections.FindAsync(collectionId);
            var livre = await _context.Livres.FindAsync(livreId);

            if (collection != null && livre != null)
            {
                var livreAjouterAMaCollection = new CollectionLivre
                {
                    LivreId = livre.Id,
                    CollectionId = collection.Id,
                    Lu = false,
                    DateCreation = DateTime.Now,
                    DateModification = DateTime.Now,
                    Collection = collection,
                    Livre = livre
                };

                var livreDejaDansLaCollection = _context.CollectionLivres.FirstOrDefault(cl => cl.CollectionId == livreAjouterAMaCollection.CollectionId
                                                                                         && cl.LivreId == livreAjouterAMaCollection.LivreId);

                if (livreDejaDansLaCollection == null)
                {
                    _context.CollectionLivres.Add(livreAjouterAMaCollection);
                    await _context.SaveChangesAsync();

                    ViewBag.MessageSucces = "Le livre a été ajouté à la collection avec succès.";
                }
                else
                {
                    ViewBag.Message = "Le livre est déjà présent dans la collection.";
                }


            }
            else
            {
                ViewBag.Message = "La collection ou le livre spécifié n'a pas été trouvé.";
            }

            Utilisateur utilisateurConnecte = JsonConvert.DeserializeObject<Utilisateur>(HttpContext.Session.GetString("Utilisateur"));
            var collections = _context.Collections
                .Where(c => c.UtilisateurId == utilisateurConnecte.Id)
                .ToList();
            ViewBag.Collections = collections;

            return View("AjouterLivre");
        }


        public IActionResult AjouterLivreFormulairePrerempli(string isbn, string titre, string auteur, DateTime? datePublication, string editeur, string description)
        {
            var livre = new Livre
            {
                Isbn = isbn,
                Titre = titre,
                Auteur = auteur,
                DateDePublication = datePublication,
                Editeur = editeur,
                Description = description
            };

            return View(livre);
        }

        // GET: Livre/AjouterLivreFormulaire
        public IActionResult AjouterLivreFormulaire()
        {


            return View();
        }

        // POST: Livre/AjouterLivre
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AjouterLivreFormulaire([Bind("Id,Isbn,Titre,Auteur,AuteurPrenom,Editeur,DateDePublication,Description,Image,DateCreation,DateModification,DateSuppression")] Livre livre)
        {

            if (ModelState.IsValid)
            {

                if (!string.IsNullOrEmpty(livre.AuteurPrenom))
                {
                    livre.Auteur = livre.AuteurPrenom + " " + livre.Auteur;
                    Console.WriteLine("auteur prenom + nom - " + livre.Auteur);
                }


                var livreExistant = await _context.Livres.FirstOrDefaultAsync(l => l.Isbn == livre.Isbn);
                if (livreExistant != null)
                {
                    ModelState.AddModelError(nameof(livre.Isbn), "Un livre avec cet ISBN existe déjà.");
                    return View(livre);
                }


                livre.DateCreation = DateTime.Now;
                livre.DateModification = DateTime.Now;

                _context.Add(livre);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(livre);
        }

        // GET: Livre/EditerLivre/5
        public async Task<IActionResult> EditerLivre(int? id)
        {
            if (id == null || _context.Livres == null)
            {
                return NotFound();
            }

            var livre = await _context.Livres.FindAsync(id);
            if (livre == null)
            {
                return NotFound();
            }
            return View(livre);
        }

        // POST: Livre/EditerLivre/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditerLivre(int id, [Bind("Id,Isbn,Titre,Auteur,Editeur,DateDePublication,Description,Image,DateModification")] Livre livre)
        {

            // penser a verifier l'isbn si il existe en cas de modification et si c'est le même que l'actuel alors c'est ok


            if (id != livre.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                //methode pour voir l'isbn si il existe (on va chercher les isbn mais pas celui du livre actuel via son id)
                var livreExistant = await _context.Livres.FirstOrDefaultAsync(l => l.Isbn == livre.Isbn && l.Id != livre.Id);
                if (livreExistant != null)
                {
                    ModelState.AddModelError(nameof(livre.Isbn), "Un livre avec cet ISBN existe déjà.");
                    return View(livre);
                }



                try
                {
                    //mise a jour de la date de modification 
                    livre.DateModification = DateTime.Now;

                    _context.Update(livre);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LivreExists(livre.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(livre);
        }

        // GET: Livre/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Livres == null)
            {
                return NotFound();
            }

            var livre = await _context.Livres
                .FirstOrDefaultAsync(m => m.Id == id);
            if (livre == null)
            {
                return NotFound();
            }

            return View(livre);
        }

        // POST: Livre/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Livres == null)
            {
                return Problem("Entity set 'BookdoubddContext.Livres'  is null.");
            }
            var livre = await _context.Livres.FindAsync(id);
            if (livre != null)
            {
                _context.Livres.Remove(livre);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LivreExists(int id)
        {
            return (_context.Livres?.Any(e => e.Id == id)).GetValueOrDefault();
        }



        //j'ai test de refactoriser mais je m'y perd.. 
        ///// <summary>
        ///// verification de l'isbn
        ///// </summary>
        ///// <param name="isbn">code isbn d'un livre</param>
        ///// <returns>message d'erreur si l'isbn est deja en base de donnée</returns>
        //private async Task<string> VerifierIsbnExistant(string isbn)
        //{
        //    var livreExistant = await _context.Livres.FirstOrDefaultAsync(l => l.Isbn == isbn);
        //    if (livreExistant != null)
        //    {
        //        return "Un livre avec cet ISBN existe déjà.";
        //    }

        //    return null;
        //}
    }
}
