using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BookDou.Models;
using Newtonsoft.Json;

namespace BookDou.Controllers
{
    public class CollectionController : Controller
    {
        private readonly BookdoubddContext _context;

        public CollectionController(BookdoubddContext context)
        {
            _context = context;
        }

        // GET: Collection
        public async Task<IActionResult> MesCollections()
        {


            Utilisateur utilisateurConnecte = JsonConvert.DeserializeObject<Utilisateur>(HttpContext.Session.GetString("Utilisateur"));

            ViewData["UtilisateurConnecte"] = utilisateurConnecte;
            var bookdoubddContext = _context.Collections
            .Include(c => c.CollectionLivres)
              .ThenInclude(cl => cl.Livre)
            .Where(c => c.Utilisateur.Id == utilisateurConnecte.Id && c.DateSuppression == null);

            return View(await bookdoubddContext.ToListAsync());
        }

        // GET: Collection/Details/5
        public async Task<IActionResult> Details(int? id)
        {

            if (id == null || _context.Collections == null)
            {
                return NotFound();
            }

            if(HttpContext != null)
            {
                string utilisateurAsJson = HttpContext.Session.GetString("Utilisateur");
            }
            

            Utilisateur utilisateurConnecte = JsonConvert.DeserializeObject<Utilisateur>(HttpContext.Session.GetString("Utilisateur"));


            var collection = await _context.Collections
                .Include(c => c.Utilisateur)
                .Include(c => c.CollectionLivres)
                    .ThenInclude(cl => cl.Livre)
                .FirstOrDefaultAsync(c => c.Id == id && c.Utilisateur.Id == utilisateurConnecte.Id && c.DateSuppression == null);

            if (collection == null)
            {
                return NotFound();
            }
         

            return View(collection);
        }


        // GET: Collection/AjouterCollection
        public IActionResult AjouterCollection()
        {

            ViewData["UtilisateurConnecte"] = JsonConvert.DeserializeObject<Utilisateur>(HttpContext.Session.GetString("Utilisateur"));
            return View();
        }

        // POST: Collection/AjouterCollection
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AjouterCollection([Bind("Id,Nom,Description,UtilisateurId")] Collection collection)
        {

            if (ModelState.IsValid)
            {
                
                collection.DateCreation = DateTime.Now;
                collection.DateModification = DateTime.Now;


                collection.Utilisateur = await _context.Utilisateurs.FindAsync(collection.UtilisateurId);



                _context.Add(collection);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(MesCollections));
            }

            return View(collection);
        }


        // GET: Collection/ModifierMaCollection/5
        public async Task<IActionResult> ModifierMaCollection(int? id)
        {
            if (id == null || _context.Collections == null)
            {
                return NotFound();
            }

            Utilisateur utilisateurConnecte = JsonConvert.DeserializeObject<Utilisateur>(HttpContext.Session.GetString("Utilisateur"));
            ViewData["UtilisateurConnecte"] = utilisateurConnecte;


            var collection = await _context.Collections.FirstOrDefaultAsync(c => c.Id == id && c.Utilisateur.Id == utilisateurConnecte.Id && c.DateSuppression == null);

            if (collection == null)
            {
                return NotFound();
            }

            return View(collection);
        }


        // POST: Collection/ModifierMaCollection/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ModifierMaCollection(int id, [Bind("Id,Nom,Description")] Collection collection)
        {

            if (id != collection.Id)
            {
                return NotFound();
            }


            if (ModelState.IsValid)
            {
                //creer ancienne colection
                var collectionMiseAJour = await _context.Collections.FirstOrDefaultAsync(c => c.Id == id);

                //set les changment de la nouvelle vers l'ancienne
                collectionMiseAJour.Nom = collection.Nom;
                collectionMiseAJour.Description = collection.Description;
                collectionMiseAJour.DateModification = DateTime.Now;


                try
                {

                    _context.Update(collectionMiseAJour);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CollectionExists(collectionMiseAJour.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(MesCollections));
            }
            return View(collection);
        }

        // GET: Collection/SupprimerCollection/5
        public async Task<IActionResult> SupprimerCollection(int? id)
        {
            //seulement si je suis l'utilisateur connecter
            Utilisateur utilisateurConnecte = JsonConvert.DeserializeObject<Utilisateur>(HttpContext.Session.GetString("Utilisateur"));
            ViewData["UtilisateurConnecte"] = utilisateurConnecte;



            if (id == null || _context.Collections == null)
            {
                return NotFound();
            }

            var collection = await _context.Collections
                .Include(c => c.Utilisateur)
                .FirstOrDefaultAsync(c => c.Id == id && c.DateSuppression == null);

            if (collection == null || collection.UtilisateurId != utilisateurConnecte.Id)
            {
                return NotFound();
            }

            return View(collection);
        }

        // POST: Collection/SupprimerCollection/5
        [HttpPost, ActionName("SupprimerCollection")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SupprimerCollectionConfirmer(int id)
        {

            //on supprime le fait de faire suppression physique et on update la date supression

            if (_context.Collections == null)
            {
                return Problem("Entity set 'BookdoubddContext.Collections'  is null.");
            }
            var collection = await _context.Collections.FindAsync(id);
            if (collection != null)
            {
                //update date suppression

                collection.DateSuppression = DateTime.Now;
                _context.Update(collection);

                //pour suppresion physique :   _context.Collections.Remove(collection);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(MesCollections));
        }

        private bool CollectionExists(int id)
        {
            return (_context.Collections?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
