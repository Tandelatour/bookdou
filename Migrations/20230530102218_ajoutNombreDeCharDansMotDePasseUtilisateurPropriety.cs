﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookDou.Migrations
{
    /// <inheritdoc />
    public partial class ajoutNombreDeCharDansMotDePasseUtilisateurPropriety : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "mot_de_passe",
                table: "utilisateur",
                type: "character varying(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(50)",
                oldMaxLength: 50);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "mot_de_passe",
                table: "utilisateur",
                type: "character varying(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(100)",
                oldMaxLength: 100);
        }
    }
}
