﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookDou.Migrations
{
    /// <inheritdoc />
    public partial class ChangementUnicitePropriety : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_livre_isbn",
                table: "livre",
                column: "isbn",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_livre_isbn",
                table: "livre");
        }
    }
}
