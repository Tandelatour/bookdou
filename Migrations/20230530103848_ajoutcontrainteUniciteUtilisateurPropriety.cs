﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookDou.Migrations
{
    /// <inheritdoc />
    public partial class ajoutcontrainteUniciteUtilisateurPropriety : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_utilisateur_email",
                table: "utilisateur",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_utilisateur_login",
                table: "utilisateur",
                column: "login",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_utilisateur_email",
                table: "utilisateur");

            migrationBuilder.DropIndex(
                name: "IX_utilisateur_login",
                table: "utilisateur");
        }
    }
}
