﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace BookDou.Migrations
{
    /// <inheritdoc />
    public partial class RenameLuFinalProperty : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "genre",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    libelle = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("genre_pkey", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "livre",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    isbn = table.Column<string>(type: "character varying(13)", maxLength: 13, nullable: false),
                    titre = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    auteur = table.Column<string>(type: "character varying(80)", maxLength: 80, nullable: false),
                    editeur = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    date_de_publication = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    description = table.Column<string>(type: "text", nullable: true),
                    image = table.Column<string>(type: "character varying", nullable: true),
                    date_creation = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    date_modification = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    date_suppression = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("livre_pkey", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "utilisateur",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    email = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    login = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    mot_de_passe = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    nom = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    prenom = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    image_profil = table.Column<string>(type: "character varying", nullable: true),
                    date_creation = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    date_modification = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    date_suppression = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("utilisateur_pkey", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "genre_livre",
                columns: table => new
                {
                    genre_id = table.Column<int>(type: "integer", nullable: false),
                    livre_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("genre_livre_pkey", x => new { x.genre_id, x.livre_id });
                    table.ForeignKey(
                        name: "fk_genre_livre_genre",
                        column: x => x.genre_id,
                        principalTable: "genre",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_genre_livre_livre",
                        column: x => x.livre_id,
                        principalTable: "livre",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "collection",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    nom = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    description = table.Column<string>(type: "text", nullable: true),
                    utilisateur_id = table.Column<int>(type: "integer", nullable: false),
                    date_creation = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    date_modification = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    date_suppression = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("collection_pkey", x => x.id);
                    table.ForeignKey(
                        name: "fk_collection_utilisateur",
                        column: x => x.utilisateur_id,
                        principalTable: "utilisateur",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "collection_livre",
                columns: table => new
                {
                    collection_id = table.Column<int>(type: "integer", nullable: false),
                    livre_id = table.Column<int>(type: "integer", nullable: false),
                    lu = table.Column<bool>(type: "boolean", nullable: false),
                    date_creation = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    date_modification = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    date_suppression = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("collection_livre_pkey", x => new { x.livre_id, x.collection_id });
                    table.ForeignKey(
                        name: "fk_collection_livre_collection",
                        column: x => x.collection_id,
                        principalTable: "collection",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_collection_livre_livre",
                        column: x => x.livre_id,
                        principalTable: "livre",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_collection_utilisateur_id",
                table: "collection",
                column: "utilisateur_id");

            migrationBuilder.CreateIndex(
                name: "IX_collection_livre_collection_id",
                table: "collection_livre",
                column: "collection_id");

            migrationBuilder.CreateIndex(
                name: "IX_genre_livre_livre_id",
                table: "genre_livre",
                column: "livre_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "collection_livre");

            migrationBuilder.DropTable(
                name: "genre_livre");

            migrationBuilder.DropTable(
                name: "collection");

            migrationBuilder.DropTable(
                name: "genre");

            migrationBuilder.DropTable(
                name: "livre");

            migrationBuilder.DropTable(
                name: "utilisateur");
        }
    }
}
