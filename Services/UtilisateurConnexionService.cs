﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookDou.Services
{
	public class UtilisateurConnexionService
	{

        [Required(ErrorMessage = "Le champ Login/Email est obligatoire.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Le champ Login doit être compris entre 3 et 50 caractères.")]
        [Display(Name = "Login / Email")]
        [RegularExpression(@"^[a-zA-Z0-9\-_@.]+$", ErrorMessage= "Veuillez entrer une adresse e-mail ou un login valide")]
        public string LoginEmail { get; set; } = null!;

        [Display(Name = "Mot de passe")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Le champ Mot de Passe est obligatoire.")]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Le champ Mot de passe doit être compris entre 6 et 50 caractères.")]
        public string MotDePasse { get; set; } = null!;

    }
}

